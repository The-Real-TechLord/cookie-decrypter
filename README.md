# Cookie Decrypter

A Burp Suite Professional extension for decrypting/decoding Citrix Netscaler and F5 BigIP persistence cookies.

* Passive scanner checks create informational issues in Burp Suite
* Written in Python
* Requires Jython 2.7+
* Pull requests welcome!

### Screenshots
[![Example Netscaler Issue](screenshots/cookie-decrypter-issue1.png)]

[![Example BigIP Issue](screenshots/cookie-decrypter-issue2.png)]

### Todo
* In Progress: Add Flask session decrypt